# Docker - Jenkins

> docker build - ajetaan kansiosta, jossa Dockerfile

```shell
docker build -t jenkins-image .
```

> kontti käyntiin projektin root-kansiosta

```shell
docker run -d --name jenkins_kontti -v ${PWD}/ssh/:/var/jenkins_home/.ssh -p 8083:8080 jenkins-image
```

> jenkins-salasana

```shell
docker exec -it jenkins_kontti cat /var/jenkins_home/secrets/initialAdminPassword
```

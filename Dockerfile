FROM jenkins/jenkins:lts

USER root

RUN apt-get update

ENV NVM_DIR /root/.nvm 

# install curl --> get nvm --> install node
RUN apt-get install curl wget && \
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.1/install.sh | bash \
    && . $NVM_DIR/nvm.sh \
    && nvm install node \
    && nvm use node

#install google-chrome dependencies
RUN apt-get -y install libxss1 lsb-release xdg-utils fonts-liberation libappindicator3-1 \
    libasound2  libatk-bridge2.0-0 libatk1.0-0 libatspi2.0-0 libcairo2 libcups2 \
    libgdk-pixbuf2.0-0 libgtk-3-0 libnspr4 libnss3 libpango-1.0-0 libpangocairo-1.0-0

# install google-chrome (for e2e testcafe)
RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb \
    && dpkg -i --force-depends google-chrome-stable_current_amd64.deb 

USER jenkins

ENV JENKINS_USER admin
ENV JENKINS_PASS admin

# install jenkins plugins: git, nodejs
RUN /usr/local/bin/install-plugins.sh git nodejs

VOLUME /var/jenkins_home

EXPOSE 8080

#RUN service jenkins start


